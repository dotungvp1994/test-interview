const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({title: String, slug: String, email: String});
module.exports = mongoose.model('categories', categorySchema);
