const mongoose = require('mongoose');

const appointmentSchema = new mongoose.Schema({
    senderName: String,
    senderPhone: String,
    senderEmail: String,
    code: String,
    supportCount: String,
    scheduleTime: Date,
    viewAt: Date,
    type: {
        type: String,
        enum: ['STREAMING', 'DIRECT']
    },
    status: {
        type: Number,
        enum: [
            0, 1, 2, 3
        ],
        default: 0
    },
    note: String,
    receiveUserId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    }
});
module.exports = mongoose.model('requests', appointmentSchema);
