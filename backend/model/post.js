const mongoose = require('mongoose');
const postSchema = new mongoose.Schema({
    code: String,
    title: String,
    slug: String,
    content: String,
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    crawl: Boolean,
    category: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'categories'
        }
    ]
});
module.exports = mongo.model('news', postSchema);
