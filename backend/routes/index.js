var express = require('express');
var router = express.Router();
const AppointmentRepository = require('../repository/appointment')
const repo = new AppointmentRepository();
router.get('/', async function (req, res, next) {
    const appointments = await repo.getByCondition(req.query)
    res.json(appointments).status(200);
});
module.exports = router;
