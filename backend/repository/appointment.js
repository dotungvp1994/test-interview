const Appointment = require('../model/appointment');
class AppointmentRepository {
    async getByCondition(requestData) {
        try {
            let query = this._initQuery();
            query = this._appendSort(query, requestData);
            query = this._appendFilter(query, requestData);
            const appointments = await Appointment.aggregate(query)
            return appointments;
        } catch (error) {
            throw error
        }
    }
    _initQuery() {
        const aggregateConditions = [
            {
                $lookup: {
                    from: "news",
                    localField: "receiveUserId",
                    foreignField: "creator",
                    as: "post"
                }
            }, {
                $lookup: {
                    from: 'users',
                    localField: 'receiveUserId',
                    foreignField: '_id',
                    as: 'creator'
                }
            }, {
                $unwind: "$post"
            }, {
                $unwind: "$creator"
            }
        ]
        return aggregateConditions
    }
    _appendFilter(query, requestData) {
        const filter = JSON.parse(requestData.filter)
        let filterObj = {}
        filterObj = this._filterByDate(filterObj, filter)
        filterObj = this._filterByState(filterObj, filter)
        filterObj = this._filterByKeyword(filterObj, filter)
        query.push({$match: filterObj})
        return query
    }
    _appendSort(query, requestData) {
        const orderCol = requestData.column ? requestData.column : 'createdAt'
        const orderDir = requestData.direction ? parseInt(requestData.direction) : -1
        let sortObj = {}
        sortObj[orderCol] = orderDir
        query.push({$sort: sortObj})
        return query
    }
    _filterByDate(filterObj, filter) {
        if (filter.view_start_date || filter.view_end_date) {
            if (!filterObj.hasOwnProperty('viewAt')) {
                filterObj.viewAt = {}
            }
            if (filter.view_start_date) {
                filterObj.viewAt = Object.assign(filterObj.viewAt, {
                    $gte: new Date(`${
                        filter.view_start_date
                    } 00:00:00`)
                })
            }
            if (filter.view_end_date) {
                filterObj.viewAt = Object.assign(filterObj.viewAt, {
                    $lte: new Date(`${
                        filter.view_end_date
                    } 23:59:59`)
                })
            }
        }
        if (filter.appointment_start_date || filter.appointment_end_date) {
            if (!filterObj.hasOwnProperty('createdAt')) {
                filterObj.createdAt = {}
            }
            if (filter.appointment_start_date) {
                filterObj.createdAt = Object.assign(filterObj.createdAt, {
                    $lte: new Date(`${
                        filter.appointment_start_date
                    } 23:59:59`)
                })
            }
            if (filter.appointment_end_date) {
                filterObj.createdAt = Object.assign(filterObj.createdAt, {
                    $lte: new Date(`${
                        filter.appointment_end_date
                    } 23:59:59`)
                })
            }
        }
        return filterObj
    }
    _filterByState(filterObj, filter) {
        if (filter.status) {
            filterObj.status = filter.status ? parseInt(filter.status) : 0
        }
        if (filter.type) {
            filterObj.type = filter.type ? parseInt(filter.type) : 0
        }
        if (filter.support_count) {
            filterObj.supportCount = filter.support_count ? parseInt(filter.support_count) : 0
        }
        if (filter.post_type) {
            filterObj['post.crawl'] = {
                $eq: filter.post_type === 'true'
            }
        }
        return filterObj
    }
    _filterByKeyword(filterObj, filter) {
        filterObj = this._filterCustomer(filterObj, filter)
        filterObj = this._filterPost(filterObj, filter)
        return filterObj
    }
    _filterCustomer(filterObj, filter) {
        if (filter.customer_search) {
            filterObj.$or = [
                {
                    senderName: {
                        '$regex': `${
                            filter.customer_search
                        }`,
                        '$options': 'i'
                    }
                }, {
                    senderPhone: {
                        '$regex': `${
                            filter.customer_search
                        }`,
                        '$options': 'i'
                    }
                }, {
                    senderEmail: {
                        '$regex': `${
                            filter.customer_search
                        }`,
                        '$options': 'i'
                    }
                },
            ]
        }
        return filterObj
    }
    _filterPost(filterObj, filter) {
        if (filter.news_search) {
            filterObj.$or = [
                {
                    'post.code': {
                        '$regex': `${
                            filter.news_search
                        }`,
                        '$options': 'i'
                    }
                }, {
                    'post.title': {
                        '$regex': `${
                            filter.news_search
                        }`,
                        '$options': 'i'
                    }
                }
            ]
        }
        return filterObj
    }
}
module.exports = AppointmentRepository
