import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faSort,
    faSortUp,
    faSortDown,
    faEye,
    faEdit,
    faTrashAlt,
    faUndo
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(faSort, faSortUp, faSortDown, faEye, faEdit, faTrashAlt, faUndo)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(VueAxios, axios)

new Vue({
    render: h => h(App)
}).$mount('#app')
